# btpanel-v7.7.0
btpanel-v7.7.0-backup  官方原版v7.7.0版本面板备份

**Centos/Ubuntu/Debian安装命令 独立运行环境（py3.7）**

```Bash
curl -sSO https://gitlab.com/ohmyga/btpanel-v7.7.0/-/raw/main/install/install_panel.sh && bash install_panel.sh
```

**去除登录框的命令**
```Bash
sed -i "s|if (bind_user == 'True') {|if (bind_user == 'REMOVED') {|g" /www/server/panel/BTPanel/static/js/index.js

rm -rf /www/server/panel/data/bind.pl
```

**VIP**
```Bash
curl -sSo /www/server/panel/class/panelPlugin.py https://gitlab.com/ohmyga/btpanel-v7.7.0/-/raw/main/panelPlugin.py
```